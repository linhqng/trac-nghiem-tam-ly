import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator } from 'react-native';
import { WebView } from 'react-native-webview';

export default function App() {
   const renderLoading = () => <ActivityIndicator style={{flex:1}} animating color="black" size="large" />
  return (


    <WebView
       startInLoadingState={true}
       renderLoading={renderLoading}
      source={{ uri: 'https://facebook.com/' }}
      style={{ marginTop: 20 }}
    />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
